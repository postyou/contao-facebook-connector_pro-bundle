<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\BackendTemplate;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\Pagination;
use Contao\Config;
use Contao\StringUtil;
use Contao\System;
use Contao\ContentElement;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnectorHelper;

class FacebookEventList extends ContentElement
{
    protected $strTemplate = 'mod_facebook_events';

    public function __construct($objModule, $strColumn = 'main')
    {
        $GLOBALS['TL_JAVASCRIPT']['video'] = 'system/modules/contao-facebook-connector_basic/assets/js/video.js';
        parent::__construct($objModule, $strColumn);
    }

    public function generate()
    {
        // Backend Ausgabe
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### ' . utf8_strtoupper("Facebook Post List") . ' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&table=tl_module&act=edit&id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    protected function compile()
    {
        $limit = null;
        $offset = 0;
        $count = 0;

        $siteIds = unserialize($this->facebookSites);
        if (!empty($siteIds)) {


            $eventModels = FacebookEventsModel::findBy(
                array(
                    'pid IN (' . implode(',', $siteIds) . ') AND published = "1"'
                ), null, array('order' => 'created_time DESC', 'limit' => $this->Events));

            $objTemplate = new FrontendTemplate('ce_facebook_events');

            $total = count($eventModels);

            // Split the results
            if ($this->perPage > 0) {

                // Get the current page
                $id = 'page_n' . $this->id;
                $page = (Input::get($id) !== null) ? Input::get($id) : 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total / $this->perPage), 1)) {
                    /** @var \PageModel $objPage */
                    global $objPage;

                    /** @var \PageError404 $objHandler */
                    $objHandler = new $GLOBALS['TL_PTY']['error_404']();
                    $objHandler->generate($objPage->id);
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset += (max($page, 1) - 1) * $this->perPage;

                // Overall limit
                if ($offset + $limit > $total) {
                    $limit = $total - $offset;
                }

                // Adjust the overall limit
                if (isset($limit)) {
                    $total = min($limit, $total);
                }

                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage,
                    Config::get('maxPaginationLinks'), $id);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }

            $events = array();

            $eventModels = FacebookEventsModel::findBy(
                array(
                    'pid IN (' . implode(',', $siteIds) . ') AND published = "1"'
                ), null,
                array(
                    'offset' => $offset,
                    'limit' => isset($limit) ? $limit : $this->maxEvents,
                    'order' => 'created_time DESC'
                ));

        }
        if (!empty($eventModels)) {
            while ($eventModels->next()) {

                $objTemplate->title = $eventModels->current()->title !== '-' ? $eventModels->current()->title : '';

                $objTemplate->facebookLinkHref = $eventModels->current()->facebookLink;

                //Textlaenge kuerzen
                if (!empty($this->messageLength) && (strlen($eventModels->current()->postMessage) > $this->messageLength)) {
                    $objTemplate->message = StringUtil::substr($eventModels->current()->postMessage, $this->messageLength,
                ' ...');
                    System::loadLanguageFile('tl_facebook_events');

                    $objTemplate->facebookLink = '<a target="_blank" href="'.$eventModels->current()->facebookLink.'">'.$GLOBALS['TL_LANG']['tl_facebook_events']['facebookLinkText'].'</a>';
                } else {
                    $objTemplate->message = $eventModels->current()->description;
                    if ($this->showFacebookLinkAlways) {
                        System::loadLanguageFile('tl_facebook_events');
                        $objTemplate->facebookLink = '<a target="_blank" href="'.$eventModels->current()->facebookLink.'">'.$GLOBALS['TL_LANG']['tl_facebook_events']['facebookLinkText'].'</a>';
                    }
                }



                // Link Erkennung
                $objTemplate->message = FbConnectorHelper::autolink($objTemplate->message, array('target' => '_blank'));

                //Hash Tag Entfernen
                if ($eventModels->current()->removeHashTag) {
                    $objTemplate->message = FbConnectorHelper::removeHashTag($objTemplate->message);
                }

                // auf null setzen, da Template sonst Wert vom vorhergehenden uebernimmt
                $objTemplate->imageSrcFacebook = null;


                $objTemplate->floatClass = $eventModels->current()->floating;

                $objTemplate->updatedTime = date(Config::get('datimFormat'),
                    $eventModels->current()->updated_time);

                $objTemplate->createdTime = date(Config::get('datimFormat'),
                    $eventModels->current()->created_time);


                $cssID = 'facebook-event-' . $count;
                $objTemplate->cssID = $cssID;

                $objTemplate->class = 'facebook-event block ' . ((++ $count == 1) ? ' first' : '') .
                     (($count == $total) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even');

                $objTemplate->beforeStyle = null;

                $events[] = $objTemplate->parse();
            }
        }
        $this->Template->events = $events;
        $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];
    }
}
