<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\ContentElement;
use Contao\BackendTemplate;
use Contao\FrontendTemplate;
use Contao\FilesModel;
use Contao\File;
use Contao\Input;
use Contao\Pagination;
use stdClass;

class FacebookGallery extends ContentElement
{
    protected $strTemplate = 'mod_facebook_gallery';

    public function __construct($objModule, $strColumn = 'main')
    {
        parent::__construct($objModule, $strColumn);
    }

    public function generate()
    {
        // Backend Ausgabe
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### ' . utf8_strtoupper("Facebook Gallery") . ' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&table=tl_module&act=edit&id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    protected function compile()
    {
        $limit = null;
        $offset = 0;

        $galleryId = $this->facebookGallery;

        $galleryModel = FacebookGalleriesModel::findById($galleryId, array('order' => 'created_time DESC'));

        $objTemplate = new FrontendTemplate('ce_facebook_gallery');


        $multiSRC = unserialize($galleryModel->multiSRC);



        $images = array();

        $total = 0;
        if (!empty($multiSRC) && is_array($multiSRC)) {
            if ($this->numberOfItems > 0) {
                $multiSRC = array_slice($multiSRC, 0, $this->numberOfItems);
            }
            foreach ($multiSRC as $uuid) {
                $fileModel = FilesModel::findByUuid($uuid);

                $objFile = new File($fileModel->path, true);

                if (!$objFile->isImage) {
                    continue;
                }

                $images[] = array(
                                'singleSRC' => $fileModel->path,
                                'id' => $fileModel->id,
                                'uuid' => $filesModel->uuid,
                                'imageUrl'  => $fileModel->path,
                                'alt'  => substr($fileModel->path, strrpos($fileModel->path, DIRECTORY_SEPARATOR) + 1),
                            );
                $total++;
            }

            $limit = $total;

            // Split the results
            if ($this->perPage > 0) {


                // Get the current page
                $id = 'page_n' . $this->id;
                $page = (Input::get($id) !== null) ? \Input::get($id) : 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total / $this->perPage), 1)) {
                    /** @var \PageModel $objPage */
                    global $objPage;

                    /** @var \PageError404 $objHandler */
                    $objHandler = new $GLOBALS['TL_PTY']['error_404']();
                    $objHandler->generate($objPage->id);
                }

                // Set limit and offset
                $offset = ($page - 1) * $this->perPage;
                $limit = min($this->perPage + $offset, $total);


                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage,
                    \Config::get('maxPaginationLinks'), $id);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }


            $rowcount = 0;
            $colwidth = floor(100/$this->perRow);
            $intMaxWidth = (TL_MODE == 'BE') ? floor((640 / $this->perRow)) : floor((\Config::get('maxImageWidth') / $this->perRow));
            $strLightboxId = 'lightbox[lb' . $this->id . ']';
            $body = array();

            // Rows
            for ($i=$offset; $i<$limit; $i=($i+$this->perRow)) {
                $class_tr = '';

                if ($rowcount == 0) {
                    $class_tr .= ' row_first';
                }

                if (($i + $this->perRow) >= $limit) {
                    $class_tr .= ' row_last';
                }

                $class_eo = (($rowcount % 2) == 0) ? ' even' : ' odd';

                // Columns
                for ($j=0; $j<$this->perRow; $j++) {
                    $class_td = '';

                    if ($j == 0) {
                        $class_td .= ' col_first';
                    }

                    if ($j == ($this->perRow - 1)) {
                        $class_td .= ' col_last';
                    }

                    $objCell = new stdClass();
                    $key = 'row_' . $rowcount . $class_tr . $class_eo;

                    // Empty cell
                    if (!is_array($images[($i+$j)]) || ($j+$i) >= $limit) {
                        $objCell->colWidth = $colwidth . '%';
                        $objCell->class = 'col_'.$j . $class_td;
                    } else {
                        $images[($i+$j)]['size'] = $this->sizeFacebook;
                        $images[($i+$j)]['imagemargin'] = $this->imagemargin;
                        $images[($i+$j)]['fullsize'] = true;

                        $this->addImageToTemplate($objCell, $images[($i+$j)], $intMaxWidth, $strLightboxId);


                        $objCell->colWidth = $colwidth . '%';
                        $objCell->class = 'col_'.$j . $class_td;
                    }

                    $body[$key][$j] = $objCell;
                }

                ++$rowcount;
            }

            $objTemplate->body = $body;
            $objTemplate->headline = $this->headline;
            $objTemplate->perRow = $this->perRow;

            // $objTemplate->setData($this->arrData);

            $this->Template->images = $objTemplate->parse();
        }
    }
}
