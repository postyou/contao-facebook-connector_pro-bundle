<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

class EventListFacebookHook
{
    public function formatTeaser($arrEvents, $arrCalendars, $intStart, $intEnd, $objModule)
    {
        foreach ($arrEvents as &$eventsGroupedByDay) {
            foreach ($eventsGroupedByDay as &$eventsGroupedByTime) {
                foreach ($eventsGroupedByTime as &$event) {
                    $event['details']  = $event['teaser'];
        //Textlaenge kuerzen
          if (!empty($objModule->messageLength) && (strlen($event['teaser']) > $objModule->messageLength)) {
              $event['teaser'] = \StringUtil::substr($event['teaser'], $objModule->messageLength, ' ...');
          }

                    //Link Erkennung
                    $event['teaser'] = FbConnectorHelper::autolink($event['teaser'], array('target' => '_blank'));
                    $event['details'] = FbConnectorHelper::autolink($event['details'], array('target' => '_blank'));

                    //Hash Tag Entfernen
                    $event['teaser'] = FbConnectorHelper::removeHashTag($event['teaser']);
                    $event['details'] = FbConnectorHelper::removeHashTag($event['details']);
                }
            }
        }


        return $arrEvents;
    }
}
