<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\Controller;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnectorHelper;
use Postyou\ContaoFacebookConnectorBasicBundle\ConnectionType;
use Postyou\ContaoFacebookConnectorBasicBundle\BackendAjaxHook;

class BackendAjaxHookPro extends BackendAjaxHook
{
    public function executePreActions($strAction)
    {
        $session = \System::getContainer()->get('session');
        if (! FbConnectorHelper::isFacebookSitesSession()) {
            $session->set('tl_facebook_sites', array());
        }


        switch ($strAction) {
            case 'loadGalleries':

                $sites = $session->get('tl_facebook_sites');
                $sites['savedGalleries'] = array();
                $sites['savedGalleriesCount'] = 0;
                $session->set('tl_facebook_sites', $sites);

                parent::loadData(ConnectionType::GALLERY_GET, getGalleryDataFromSiteId);

                $url = Controller::addToUrl('&reportTypeGallery=true&directionGet=true&rt='.FbConnectorHelper::getToken(), true,
                    array(
                        'key',
                        'directionPublish',
                        'reportTypeEvent',
                        'reportTypePost'
                    ));

                 echo json_encode(html_entity_decode($url));
                 exit();

                break;

            case 'loadEvents' :

                $sites = $session->get('tl_facebook_sites');
                $sites['savedEvents'] = array();
                $sites['savedEventsCount'] = 0;
                $session->set('tl_facebook_sites', $sites);

                parent::loadData(ConnectionType::EVENTS, getEventsFromSiteIdAndSaveInDb);

                $url = Controller::addToUrl('&reportTypeEvent=true&directionGet=true&rt='.FbConnectorHelper::getToken(), true,
                    array(
                        'key',
                        'directionPublish',
                        'reportTypePost',
                        'reportTypeGallery'
                    ));

                echo json_encode(html_entity_decode($url));
                exit();

                break;

            case 'publishPosts' :

                $sites = $session->get('tl_facebook_sites');
                $sites['publishedPosts'] = array();
                $sites['publishedPostsCount'] = 0;
                $session->set('tl_facebook_sites', $sites);

                parent::loadData(ConnectionType::POST_PUBLISH, publishPostsForSiteId);
                parent::loadData(ConnectionType::POST_PUBLISH, deletePostsOnFacebook);

                $url = Controller::addToUrl('&reportTypePost=true&directionPublish=true&rt='.FbConnectorHelper::getToken(), true,
                    array(
                        'key',
                        'directionGet',
                        'reportTypeEvent',
                        'reportTypeGallery'
                    ));

                echo json_encode(html_entity_decode($url));
                exit();

                break;

            default:
                break;
        }
    }
}
