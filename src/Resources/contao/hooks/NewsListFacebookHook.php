<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

class NewsListFacebookHook
{
    public function filterFacebookPosts($newsArchives, $blnFeatured, $limit, $offset, $thisModule)
    {
        if ($thisModule->hideFacebookNews) {
            $objArchive = FacebookContaoNewsModel::findBy(array('pid IN ('.implode(',', deserialize($newsArchives)).') AND published = 1 AND isFacebookPost = "" ORDER BY time DESC LIMIT '.$limit), null);
            return $objArchive;
        }

        return $newsArchives;
    }
}
