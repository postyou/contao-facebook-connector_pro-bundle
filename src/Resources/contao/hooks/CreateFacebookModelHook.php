<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\Model\Collection;


class CreateFacebookModelHook
{
    public function createModelDependingNewsmoduleSetting($facebookSiteModel, $postId = null)
    {
        if ($facebookSiteModel->addPostsToNewsModule) {
            $newsArchives = unserialize($facebookSiteModel->newsArchive);
            $modelArray = array();
            foreach ($newsArchives as $newsArchive) {
                $model = FacebookContaoNewsModel::findByFacebookPostIdAndPid($postId, $newsArchive);

                //findByFacebookPostIdAndPid liefert Collection mit einzelobjekt zurueck,
                if ($model instanceof \Model\Collection) {
                    $tempModel = $model->current()->cloneOriginal();
                    // //Model loeschen da sonst Fehler beim Speichern auftritt (duplicate entry)
                    $model->current()->delete();
                    $model = new FacebookContaoNewsModel($tempModel);
                    $model->save();
                }


                if (empty($model)) {
                    $model = new FacebookContaoNewsModel();
                    $model->isFacebookPost = true;
                    $model->facebookPostId = $postId;
                    $model->facebookSitePid = $facebookSiteModel->id;
                    $model->pid = $newsArchive;
                    $model->published = true;
                }

                $modelArray[] = $model;
            }

            return new Collection($modelArray, 'tl_news');
        }
    }
}
