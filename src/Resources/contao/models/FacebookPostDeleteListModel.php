<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\Model;

class FacebookPostDeleteListModel extends Model
{

    /**
     * Table name
     *
     * @var string
     */
    protected static $strTable = 'tl_facebook_post_delete_list';

    public static function findByPostId($postId)
    {
        return self::findBy('postId', $postId);
    }

    public static function findByPid($pid)
    {
        return self::findBy('pid', $pid);
    }
}
