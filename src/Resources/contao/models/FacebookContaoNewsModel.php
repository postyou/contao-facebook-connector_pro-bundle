<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\NewsModel;
use Contao\Config;

class FacebookContaoNewsModel extends NewsModel
{
    public function __construct(NewsModel $newsModel = null)
    {
        if (!empty($newsModel)) {
            foreach ($newsModel as $property => $value) {
                $this->$property = $value;
            }
        }
    }

    public static function findByFacebookPostId($facebookPostId)
    {
        return self::findBy('facebookPostId', $facebookPostId,
            array(
                'order' => 'tstamp DESC'
            ));
    }

    public static function findByFacebookPostIdAndPid($facebookPostId, $pid)
    {
        return self::findBy(array("facebookPostId='".$facebookPostId."' AND pid=".$pid), null,
            array(
                'order' => 'tstamp DESC'
            ));
    }

    public static function findByPid($pid)
    {
        return self::findBy('pid', $pid, array(
            'order' => 'tstamp DESC'
        ));
    }

    public static function findByPidASC($pid)
    {
        return self::findBy('pid', $pid, array(
            'order' => 'tstamp ASC'
        ));
    }

    public function getFolderPath()
    {
        return Config::get('uploadPath') . '/facebook/posts/' . $this->facebookPostId . $this->pid;
    }

    public function addSpecificData(array $arrData)
    {
        foreach ($arrData as $key => $value) {
            switch ($key) {
                  case 'title':
                          $this->headline = $value;
                          break;
                  case 'message':
                          $this->teaser = $value;
                          break;
                  case 'dateUpdated':
                          $this->date = $value;
                          $this->time = $value;
                          break;
                  case 'addImage':
                          $this->addImage = $value;
                          break;
                  case 'source':
                          $this->source = $value;
                          break;
                  case 'singleSRC':
                          $this->singleSRC = $value;
                          break;
                  case 'fullsize':
                          $this->fullsize = $value;
                          break;
                  case 'floating':
                          $this->floating = $value;
                          break;
                  case 'multiSRC':
                          $this->multiSRC = $value;
                          break;
                  case 'facebookPostType':
                          $this->facebookPostType = $value;
                          break;
                        }
        }
    }
}
