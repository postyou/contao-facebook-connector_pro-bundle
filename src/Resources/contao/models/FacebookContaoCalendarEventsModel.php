<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\CalendarEventsModel;

class FacebookContaoCalendarEventsModel extends CalendarEventsModel
{
    public static function findByFacebookEventId($facebookEventId)
    {
        return self::findBy('facebookEventId', $facebookEventId,
            array(
                'order' => 'tstamp DESC'
            ));
    }

    public static function findByPid($pid)
    {
        return self::findBy('pid', $pid, array(
            'order' => 'tstamp DESC'
        ));
    }
}
