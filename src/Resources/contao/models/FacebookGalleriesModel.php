<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\Model;

class FacebookGalleriesModel extends Model
{

    /**
     * Table name
     *
     * @var string
     */
    protected static $strTable = 'tl_facebook_galleries';

    public static function findByAlbumId($albumId)
    {
        return self::findBy('albumId', $albumId);
    }

    public static function findByPid($pid)
    {
        return self::findBy('pid', $pid);
    }
}
