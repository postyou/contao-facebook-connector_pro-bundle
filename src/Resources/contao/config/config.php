<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

if (! defined('TL_ROOT')) {
    die('You cannot access this file directly!');
}


// BE Module
$GLOBALS['BE_MOD']['Facebook']['Facebook-Seiten']['login'] = array(
        'Postyou\ContaoFacebookConnectorProBundle\FbBackendWrapperPro',
        'login'
);

$GLOBALS['BE_MOD']['content']['news']['deleteOnFacebook'] = array(
        'Postyou\ContaoFacebookConnectorProBundle\FbBackendWrapper',
        'onDeleteFacebook'
);


$GLOBALS['TL_CTE']['Facebook']['facebook_galleries'] = 'Postyou\ContaoFacebookConnectorProBundle\FacebookGallery';
$GLOBALS['TL_CTE']['Facebook']['facebook_event_list'] = 'Postyou\ContaoFacebookConnectorProBundle\FacebookEventList';

$GLOBALS['FE_MOD']['news']['newslist_facebook'] = 'Postyou\ContaoFacebookConnectorProBundle\ModuleNewsListFacebook';
$GLOBALS['FE_MOD']['events']['eventreader_facebook'] = 'Postyou\ContaoFacebookConnectorProBundle\ModuleEventReaderFacebook';

//Hooks
$GLOBALS['TL_HOOKS']['executePreActions'][] = array('Postyou\ContaoFacebookConnectorProBundle\BackendAjaxHookPro', 'executePreActions');
$GLOBALS['TL_HOOKS']['newsListFetchItems'][] = array('Postyou\ContaoFacebookConnectorProBundle\NewsListFacebookHook', 'filterFacebookPosts');
$GLOBALS['TL_HOOKS']['createFacebookModel'][] = array('Postyou\ContaoFacebookConnectorProBundle\CreateFacebookModelHook', 'createModelDependingNewsmoduleSetting');
$GLOBALS['TL_HOOKS']['getAllEvents'][] = array('Postyou\ContaoFacebookConnectorProBundle\EventListFacebookHook', 'formatTeaser');


/*
 * Models
 */
$GLOBALS['TL_MODELS']['tl_facebook_calender_events'] = \Postyou\ContaoFacebookConnectorProBundle\FacebookContaoCalendarEventsModel::class;
$GLOBALS['TL_MODELS']['tl_facebook_contao_news'] = \Postyou\ContaoFacebookConnectorProBundle\FacebookContaoNewsModel::class;
$GLOBALS['TL_MODELS']['tl_facebook_events'] = \Postyou\ContaoFacebookConnectorProBundle\FacebookEventsModel::class;
$GLOBALS['TL_MODELS']['tl_facebook_galleries'] = \Postyou\ContaoFacebookConnectorProBundle\FacebookGalleriesModel::class;
$GLOBALS['TL_MODELS']['tl_facebook_post_delete_list'] = \Postyou\ContaoFacebookConnectorProBundle\FacebookPostDeleteListModel::class;
