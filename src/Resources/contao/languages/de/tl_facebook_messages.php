<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['tl_facebook_messages']['emptyTeaser'] = 'Der Newsmeldung enth&auml;lt keine Textinhalte. Bitte f&uuml;llen Sie das Feld "Teasertext" aus.';
$GLOBALS['TL_LANG']['tl_facebook_messages']['deleteFacebookNews'] = 'Achtung! Diese Newsmeldung wird beim n&auml;chsten Upload auch auf Facebook gel&ouml;scht.';
$GLOBALS['TL_LANG']['tl_facebook_messages']['deleteFacebookNewsContao'] = 'Achtung! Die Newsmeldung wird nur in Contao entfernt und beim n&auml;chsten Synchronisationsvorgang ggf. wieder hinzugef&uuml;gt.';
$GLOBALS['TL_LANG']['tl_facebook_messages']['deleteFacebookNewsContao2'] = 'Um das erneute Hinzuf&uuml;gen zu verhindern, l&ouml;schen Sie den Eintrag auf Facebook oder beschr&auml;nken Sie den Import auf neuere Eintr&auml;ge.';
$GLOBALS['TL_LANG']['tl_facebook_messages']['deleteConfirmNews'] = 'Soll die Newsmeldung wirklich gel&ouml;scht werden?';
$GLOBALS['TL_LANG']['tl_facebook_messages']['deleteConfirmEvents'] = 'Soll die Veranstaltung wirklick gel&ouml;scht werden?';
$GLOBALS['TL_LANG']['tl_facebook_messages']['deleteToggleInfo'] = 'Tipp: Um den Beitrag nur auf der Webseite auszublenden, benutzen Sie die &quot;ver&ouml;ffentlichen/unver&ouml;ffentlichen&quot; Funktion.';
$GLOBALS['TL_LANG']['tl_facebook_messages']['publishedInfo'] = 'Dieser Beitrag ist bereits auf Facebook ver&ouml;ffentlicht';
