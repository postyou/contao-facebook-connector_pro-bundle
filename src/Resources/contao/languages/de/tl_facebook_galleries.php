<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['tl_facebook_galleries']['title'][0] = 'Titel';
$GLOBALS['TL_LANG']['tl_facebook_galleries']['title'][1] = 'Der Titel des Albums.';

$GLOBALS['TL_LANG']['tl_facebook_galleries']['multiSRC'][0] = 'Bilder';
$GLOBALS['TL_LANG']['tl_facebook_galleries']['multiSRC'][1] = 'Die Bilder des Albums';

$GLOBALS['TL_LANG']['tl_facebook_galleries']['title_legend'] = 'Albumdaten';

// Globale Operationen
$GLOBALS['TL_LANG']['tl_facebook_galleries']['new'][0] = "Neues Album";
$GLOBALS['TL_LANG']['tl_facebook_galleries']['new'][1] = "Neues Album";
