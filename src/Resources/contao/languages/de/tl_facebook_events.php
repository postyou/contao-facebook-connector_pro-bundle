<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['tl_facebook_events']['new'][0] = 'Neues Event';
$GLOBALS['TL_LANG']['tl_facebook_events']['new'][1] = 'Neues Event';
$GLOBALS['TL_LANG']['tl_facebook_events']['title'][0] = 'Titel';
$GLOBALS['TL_LANG']['tl_facebook_events']['location'][0] = 'Veranstaltungsort';
$GLOBALS['TL_LANG']['tl_facebook_events']['description'][0] = 'Beschreibung';
$GLOBALS['TL_LANG']['tl_facebook_events']['start_time'][0] = 'Start der Veranstaltung';
$GLOBALS['TL_LANG']['tl_facebook_events']['end_time'][0] = 'Ende der Veranstaltung';
$GLOBALS['TL_LANG']['tl_facebook_events']['ticketUri'][0] = 'Ticket-URL';
