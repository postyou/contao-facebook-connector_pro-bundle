<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\FilesModel;
use Contao\Environment;
use Postyou\ContaoFacebookConnectorBasicBundle\FacebookSitesModel;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnectorHelper;
use Contao\System;
use Postyou\ContaoFacebookConnectorBasicBundle\FacebookPostsModel;
use Contao\NewsModel;
use Facebook\Exceptions\FacebookOtherException;
use DateTime;


class FbConnectorPostPublish extends FbConnectorPhpSdk
{
    private $pageAccessToken;

    public function publishPostsForSiteId($id)
    {
        $facebookSitesModel = FacebookSitesModel::findById($id);

        $pageAccessToken = null;
        try {
            $pageAccessToken = $this->getPageAccessTokenFromFacebookAlias(
                $facebookSitesModel->facebookAlias);
            $this->pageAccessToken = $pageAccessToken;
        } catch (\Exception $e) {
            $_SESSION['tl_facebook_sites']['errorMessages'][] = $e->getMessage();
            return;
        }

        $data = array();
        $dateCreated = new \DateTime();

        if ($facebookSitesModel->addPostsToNewsModule) {
            $newsArchives = unserialize($facebookSitesModel->newsArchive);
            if (! empty($newsArchives)) {
                foreach ($newsArchives as $newsArchive) {
                    $newsModelCollection = FacebookContaoNewsModel::findByPidASC($newsArchive);
                    if (! empty($newsModelCollection)) {
                        foreach ($newsModelCollection as $index => $newsModel) {

                            if (! $newsModel->isFacebookPost || strcmp($newsModel->facebookPostType, 'photo') === 0) {
                                continue;
                            }

                            $dateCreated->setTimestamp($newsModel->tstamp);

                            if (empty($newsModel->teaser)) {
                                System::loadLanguageFile('tl_facebook_messages');
                                FbConnectorHelper::addRejectedPostMessage($dateCreated, $newsModel,
                                    $GLOBALS['TL_LANG']['tl_facebook_messages']['emptyTeaser']);
                                continue;
                            }

                            $this->uploadPost($newsModel->facebookPostId, $dateCreated,
                                $facebookSitesModel->facebookAlias, $newsModel);
                        }
                    }
                }
            }
        } else {
            $facebookPostModelCollection = FacebookPostsModel::findByPid($id);
            if (! empty($facebookPostModelCollection)) {
                foreach ($facebookPostModelCollection as $index => $facebookPostModel) {
                    if (strcmp($facebookPostModel->type, 'photo') === 0) {
                        continue;
                    }

                    $dateCreated->setTimestamp($facebookPostModel->created_time);

                    $this->uploadPost($facebookPostModel->postId, $dateCreated,
                        $facebookSitesModel->facebookAlias, $facebookPostModel);
                }
            }
        }
    }

    public function deletePostsOnFacebook($siteId = null)
    {
        if (isset($siteId)) {
            $facebookSitesModel = FacebookSitesModel::findById($siteId);
            if (isset($facebookSitesModel)) {
                $this->deletePostsOnFacebookForAlias($facebookSitesModel->facebookAlias, $siteId);
            }
        } else {
            $facebookSitesModelCollection = FacebookSitesModel::findAll();
            if (! empty($facebookSitesModelCollection)) {
                foreach ($facebookSitesModelCollection as $facebookSitesModel) {
                    $this->deletePostsOnFacebookForAlias($facebookSitesModel->facebookAlias,
                        $facebookSitesModel->id);
                }
            }
        }
    }

    private function deletePostsOnFacebookForAlias($facebookAlias, $pid)
    {
        $pageAccessToken = null;
        try {
            $pageAccessToken = $this->getPageAccessTokenFromFacebookAlias($facebookAlias);
        } catch (\Exception $e) {
            $_SESSION['tl_facebook_sites']['errorMessages'][] = $e->getMessage();
            return;
        }

        $facebookPostDeleteListModelCollection = FacebookPostDeleteListModel::findByPid($pid);
        if (isset($facebookPostDeleteListModelCollection)) {
            $deletedPostIdList = array();
            foreach ($facebookPostDeleteListModelCollection as $facebookPostDeleteListModel) {
                try {
                    $response = $this->fb->delete('/' . $facebookPostDeleteListModel->postId,
                        array(), $pageAccessToken);
                    if ($response->getDecodedBody()['success'] == true) {
                        $deletedPostIdList[] = $facebookPostDeleteListModel->postId;
                        if (! isset($_SESSION['tl_facebook_sites']['deletedPostsCount'])) {
                            $_SESSION['tl_facebook_sites']['deletedPostsCount'] = 1;
                        } else {
                            $_SESSION['tl_facebook_sites']['deletedPostsCount'] ++;
                        }
                    }
                } catch (\Exception $e) {
                    $_SESSION['tl_facebook_sites']['errorMessages'][] = $e->getMessage();
                }
            }

            foreach ($deletedPostIdList as $deletePostId) {
                $facebookPostDeleteListModel = FacebookPostDeleteListModel::findByPostId(
                    $deletePostId);
                $facebookPostDeleteListModel->delete();
            }
        }
    }

    private function uploadPost($postId, $dateCreated, $facebookAlias, $model)
    {
        try {
            $response = $this->fb->get('/' . $facebookAlias . '/posts?fields=id,message,updated_time',
                $this->pageAccessToken);

            $postsOnFacebook = $response->getDecodedBody()['data'];
            $postWasUpdated = false;
            if (count($postsOnFacebook) === 0) {
                $data = $this->buildPostDataArray($model, $facebookAlias);
                $this->publishNewPost($data, $model);
            } else {
                foreach ($postsOnFacebook as $post) {
                    $message = null;
                    if ($model instanceof FacebookPostsModel) {
                        $message = $model->postMessage;
                    } elseif ($model instanceof NewsModel) {
                        $message = $model->teaser;
                    }

                    if ($post['id'] === $postId) {
                        $dateUpdated = new DateTime($post['updated_time']);
                        if (strcmp($message, $post['message']) === 0 || $model->tstamp <= $dateUpdated->getTimestamp()) {
                            return;
                        }

                        $data = $this->buildPostDataArrayWithoutImages($model, $facebookAlias);
                        $this->fb->post($postId, $data, $this->pageAccessToken);
                        $postWasUpdated = true;
                        break;
                    }
                }

                if (! $postWasUpdated) {
                    $data = $this->buildPostDataArray($model, $facebookAlias);

                    $this->publishNewPost($data, $model);
                }
            }



            FbConnectorHelper::updateSessionValuesForResponse('publishedPostsCount',
                'publishedPosts', ((! empty($model->title)) ? $model->title : $model->headline),
                $dateCreated, $postWasUpdated);
        } catch (FacebookOtherException $e) {
            FbConnectorHelper::addRejectedPostMessage($dateCreated, $model, $e->getMessage());
        } catch (\Exception $e) {
            FbConnectorHelper::addRejectedPostMessage($dateCreated, $model, $e->getMessage());
        }
    }

    private function publishNewPost($data, $model)
    {
        $response = $this->fb->post('me/feed', $data, $this->pageAccessToken);
        $id = $response->getDecodedBody()['id'];

        if (! empty($id)) {
            if ($model instanceof FacebookPostsModel) {
                $model->postId = $id;
            } else {
                if ($model instanceof \NewsModel) {
                    $model->facebookPostId = $id;
                }
            }

            $model->save();
        }
    }

    private function buildPostDataArrayWithoutImages($model, $facebookAlias)
    {
        return $this->buildPostDataArray($model, $facebookAlias, true);
    }

    private function buildPostDataArray($model, $facebookAlias, $postWasUpdated = false)
    {
        $data = array();

        if ($model instanceof FacebookPostsModel) {
            if (empty($model->postMessage)) {
                throw new \Exception('Post hat keinen Textinhalt.');
            }

            $data['message'] = $model->postMessage;

            if ($postWasUpdated) {
                return $data;
            }

            if ($model->type === 'photo') {
                $multiSRC = unserialize($model->multiSRC);
                if (! empty($multiSRC)) {
                    foreach ($multiSRC as $index => $src) {
                        $path = FilesModel::findByUUID($src)->path;

                        $photoId = $this->uploadPhotoAndGetId(Environment::get('url') . '/' . $path,
                            $facebookAlias);

                        if (! empty($photoId)) {
                            $data['attached_media[' . $index . ']'] = '{"media_fbid":"' . $photoId .
                                 '"}';
                        }
                    }
                }
            }
        } else {
            if ($model instanceof NewsModel) {
                if (empty($model->teaser)) {
                    throw new FacebookOtherException('Post hat keinen Textinhalt.');
                }
                $data['message'] = strip_tags($model->teaser);

                if ($postWasUpdated) {
                    return $data;
                }

                if ($model->addImage == 1) {
                    $path = FilesModel::findByUUID($model->singleSRC)->path;
                    $photoId = $this->uploadPhotoAndGetId(Environment::get('url') . '/' . $path,
                        $facebookAlias);
                    $data['attached_media[0]'] = '{"media_fbid":"' . $photoId . '"}';
                }
            }
        }

        return $data;
    }

    private function uploadPhotoAndGetId($photoPath, $facebookAlias)
    {
        $response = null;
        try {
            $response = $this->fb->post($facebookAlias . '/photos',
                array(
                    'url' => $photoPath,
                    'published' => false
                ), $this->pageAccessToken);
        } catch (\Exception $e) {
            FbConnectorHelper::addErrorMessage($e);
            return;
        }

        return $response->getDecodedBody()['id'];
    }


}
