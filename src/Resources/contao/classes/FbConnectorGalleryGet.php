<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Contao\File;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnectorHelper;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnector;
use Postyou\ContaoFacebookConnectorBasicBundle\FacebookSitesModel;
use Postyou\ContaoFacebookConnectorProBundle\FacebookGalleriesModel;
use Contao\Folder;
use Contao\Config;

class FbConnectorGalleryGet extends FbConnector
{
    public function __construct($arrConfig)
    {
        parent::__construct($arrConfig);
    }

    public function getGalleriesAsOptionsFromSiteId($siteId)
    {
        $facebookSiteModel = FacebookSitesModel::findById($siteId);
        if ($facebookSiteModel) {

            try {
                $url = parent::getBaseUrl() . '/' . parent::getVersion() . '/' .
                    $facebookSiteModel->facebookAlias . '/albums?' . parent::getAccessTokenQuery() .
                    '&fields=id,name,link';

                $facebookAlbums = parent::fetchData($url)['data'];

                if (isset($facebookAlbums)) {
                    $options = array();
                    foreach ($facebookAlbums as $facebookAlbum) {
                        $options[$facebookAlbum['id']] = $facebookAlbum['name'] .
                            '&nbsp;&nbsp;<a style="text-decoration:underline;" target="_blank" href="' .
                            $facebookAlbum['link'] . '">(link)</a>';
                    }

                    return $options;
                }
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

        }
    }

    public function getGalleryDataFromAllSites()
    {
        $facebookSiteModels = FacebookSitesModel::findAll();
        if (isset($facebookSiteModels)) {
            foreach ($facebookSiteModels as $facebookSiteModel) {
                if ($facebookSiteModel) {
                    $this->getGalleryDataFromSiteId($facebookSiteModel->siteId, $facebookSiteModel);
                }
            }
        }
    }

    public function getGalleryDataFromSiteId($siteId, $facebookSiteModel = null)
    {
        if (!isset($facebookSiteModel)) {
            $facebookSiteModel = FacebookSitesModel::findById($siteId);
        }

        if ($facebookSiteModel) {
            $albumIds = unserialize($facebookSiteModel->galleries);
            if (! empty($albumIds)) {
                foreach ($albumIds as $albumId) {
                    $galleryWasUpdated = true;
                    $url = parent::getBaseUrl() . '/' . parent::getVersion() . '/' . $albumId . '/?' .
                         parent::getAccessTokenQuery() . '&fields=name,updated_time,created_time';

                    $albumData = parent::fetchData($url);

                    $url = parent::getBaseUrl() . '/' . parent::getVersion() . '/' . $albumId .
                         '/photos?' . parent::getAccessTokenQuery() .
                         '&fields=images,updated_time,created_time';

                    $responseData = parent::fetchData($url);
                    $gallery = $responseData['data'];
                    $next = $responseData['paging']['next'];

                    while ($next) {
                        $responseData = parent::fetchData($next);
                        $gallery = array_merge_recursive($gallery, $responseData['data']);
                        $next = $responseData['paging']['next'];
                    }

                    $updatedTime = new \DateTime($albumData['updated_time']);

                    $facebookGalleryModel = FacebookGalleriesModel::findByAlbumId($albumId);

                    if (empty($facebookGalleryModel)) {
                        $facebookGalleryModel = new FacebookGalleriesModel();
                        $galleryWasUpdated = false;
                        $createdTime = new \DateTime($albumData['created_time']);
                        $facebookGalleryModel->created_time = $createdTime->getTimestamp();
                    } else {
                        if ($updatedTime->getTimestamp() <= $facebookGalleryModel->updated_time) {
                            continue;
                        } else {
                            $facebookGalleryModel->updatedTime = $updatedTime->getTimestamp();
                        }
                    }
                    $multiSRC = unserialize($facebookGalleryModel->multiSRC);
                    if (empty($multiSRC)) {
                        $multiSRC = array();
                    }


                    $galleryCount = count($gallery);
                    foreach ($gallery as $index => $galleryItem) {
                        $zeroSigns = $this->getFilenameZeroSigns(strlen($galleryCount.'') - strlen(($index+1).''));
                        if (! empty($galleryItem['images'])) {
                            $image = $this->getImageWithMaxSize($galleryItem['images']);

                            $filesModel = $this->saveGalleryImageToFilesystem($albumData['id'], $albumData['name'],
                                $image, $index, $zeroSigns);

                            $multiSRC[] = $filesModel->uuid;
                            $facebookGalleryModel->multiSRC = $multiSRC;
                        }
                    }

                    $facebookGalleryModel->title = $albumData['name'];

                    $facebookGalleryModel->pid = $facebookSiteModel->id;
                    $facebookGalleryModel->albumId = $albumId;
                    $date = new \DateTime($albumData['updated_time']);
                    $facebookGalleryModel->updated_time = $date->getTimestamp();
                    $facebookGalleryModel->tstamp = $date->getTimestamp();
                    $facebookGalleryModel->save();

                    FbConnectorHelper::updateSessionValuesForResponse('savedGalleriesCount',
                        'savedGalleries', $facebookGalleryModel->title, $date, $galleryWasUpdated);
                }
            }
        }
    }

    private function saveGalleryImageToFilesystem($albumId, $albumName, $image, $index, $zeroSigns)
    {
        $index += 1;
        $folderName = str_replace(' ', '_', $albumId.'_'.$albumName);
        new Folder(Config::get('uploadPath') . '/facebook/albums/' . $folderName);
        $file = new File(
            Config::get('uploadPath') . '/facebook/albums/' . $folderName . '/'. $zeroSigns . $index .
                 $this->getFileTypeFromURL($image['source']));

        $ch = parent::initCurl($image['source']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        $file->write($data);

        $file->close();
        return $file->getModel();
    }

    private function getFileTypeFromURL($url)
    {
        return substr($url, strripos($url, '.'), 4);
    }

    private function getFilenameZeroSigns($itemCount)
    {
        $filename = '';
        while ($itemCount !== 0) {
            $filename .= '0';
            --$itemCount;
        }
        return $filename;
    }

    private function getImageWithMaxSize($images)
    {
        $maxDim = 0;
        $maxImage = array();
        foreach ($images as $image) {
            if ($image['width'] >= $image['height']) {
                if ($image['width'] > $maxDim) {
                    $maxDim = $image['width'];
                    $maxImage = $image;
                }
            } else {
                if ($image['height'] > $maxDim) {
                    $maxDim = $image['height'];
                    $maxImage = $image;
                }
            }
        }

        return $maxImage;
    }
}
