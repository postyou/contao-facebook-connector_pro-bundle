<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Postyou\ContaoFacebookConnectorBasicBundle\FbConnectorHelper;
use Postyou\ContaoFacebookConnectorBasicBundle\FacebookSitesModel;

class FbConnectorEvents extends FbConnectorPhpSdk
{
    public function getEventsForAlias($facebookAlias)
    {
        $url = null;
        if (! empty($this->limit) && $this->limit < 25) {
            $url .= '&limit=' . $this->limit;
        } else {
            $url .= '&limit=25'; // Default Value
        }

        if (! empty($this->since)) {
            $url .= '&since=' . $this->since;
        }

        $response = $this->fb->get(
            '/' . $facebookAlias .
                 '/events?fields=id,name,description,start_time,end_time,updated_time,place,cover,ticket_uri'.$url,
                $this->getPageAccessTokenFromFacebookAlias($facebookAlias));

        $facebookEvents = $response->getDecodedBody();

        $facebookEventData = $facebookEvents['data'];

        $limitCounter = count($facebookEvents['data']);
        if ($limitCounter <= $this->limit) {
            while (! empty($facebookEvents['paging']['next']) && $limitCounter < $this->limit) {
                $facebookEvents = parent::fetchData($facebookEvents['paging']['next']);

                // $limitCounter += count($facebookEvents['data']);
                if (($limitCounter + count($facebookEvents['data'])) <= $this->limit) {
                    $facebookEventData = array_merge($facebookEventData, $facebookEvents['data']);
                } else {
                    $diff = $this->limit - $limitCounter;
                    for ($i = 0; $i < $diff; $i ++) {
                        $facebookEventData[] = $facebookEvents['data'][$i];
                        $limitCounter ++;
                    }
                }
            }
        }
        return $facebookEventData;
    }

    public function getEventsFromSiteIdAndSaveInDb($id)
    {
        $facebookSiteModel = FacebookSitesModel::findById($id);
        $this->getEventDataForFacebookSite($facebookSiteModel);
    }

    public function getEventsFromAllSitesAndSaveInDb()
    {
        $faceBookSitesModelCollection = FacebookSitesModel::findAll();
        if (! empty($faceBookSitesModelCollection)) {
            foreach ($faceBookSitesModelCollection as $faceBookSiteModel) {
                $this->getEventDataForFacebookSite($faceBookSiteModel);
            }
        }
    }

    private function getEventDataForFacebookSite($facebookSiteModel)
    {
        if (! empty($facebookSiteModel->maxEventsCount)) {
            $this->setLimit($facebookSiteModel->maxEventsCount);
        }

        if (! empty($facebookSiteModel->minEventsDate)) {
            $this->setSince($facebookSiteModel->minEventsDate);
        }

        $facebookEventData = $this->getEventsForAlias($facebookSiteModel->facebookAlias);

        if (! empty($facebookEventData)) {
            foreach ($facebookEventData as $event) {

                if (!empty($event['event_times'])) {
                    foreach ($event['event_times'] as $singleEvent) {
                        $this->saveEventInDb($event, $facebookSiteModel, new \DateTime($singleEvent['start_time']), new \DateTime($singleEvent['end_time']), $singleEvent['id']);
                    }
                } else {
                    $this->saveEventInDb($event, $facebookSiteModel);
                }

            }
        }
        return $facebookEventData;
    }

    private function saveEventInDb($event, $facebookSiteModel, $startTime = null, $endTime = null, $eventId = null)
    {
        if (!isset($startTime)) {
            $startTime = new \DateTime($event['start_time']);
        }

        if (!isset($endTime)) {
            $endTime = new \DateTime($event['end_time']);
        }

        if (!isset($eventId)) {
            $eventId = $event['id'];
        }

        $facebookEventsModel = \FacebookEventsModel::findByEventId($eventId);

        $eventWasUpdated = true;
        $date = new \DateTime($event['updated_time']);

        if (empty($facebookEventsModel)) {
            $facebookEventsModel = new FacebookEventsModel();
            $eventWasUpdated = false;
        } else {
            if ($facebookEventsModel->tstamp >= $date->getTimestamp()) {
                if ($facebookSiteModel->addEventsToCalendarModule) {
                    $eventModel = \FacebookContaoCalendarEventsModel::findByFacebookEventId(
                        $facebookEventsModel->eventId);
                    if (! empty($eventModel) && ($eventModel->tstamp >= $date->getTimestamp())) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        $facebookEventsModel->title = $event['name'];
        $facebookEventsModel->description = $event['description'];
        $facebookEventsModel->pid = $facebookSiteModel->id;
        $facebookEventsModel->eventId = $eventId;
        $facebookEventsModel->updated_time = $date->getTimestamp();
        $facebookEventsModel->tstamp = $date->getTimestamp();
        $facebookEventsModel->ticketUri = $event['ticket_uri'];


        $facebookEventsModel->start_time = $startTime->getTimestamp();

        $facebookEventsModel->end_time = $endTime->getTimestamp();
        $facebookEventsModel->location = $event['place']['name'];
        if (! empty($event['cover'])) {
            $facebookEventsModel->imageSrcFacebook = $event['cover']['source'];
        }

        $facebookEventsModel = $facebookEventsModel->save();
        if ($facebookSiteModel->addEventsToCalendarModule) {
            $calendars = unserialize($facebookSiteModel->calendar);
            if (! empty($calendars)) {
                foreach ($calendars as $calendar) {
                    $eventModel = \FacebookContaoCalendarEventsModel::findByFacebookEventId(
                        $facebookEventsModel->eventId);
                    if (empty($eventModel)) {
                        $eventModel = new FacebookContaoCalendarEventsModel();
                        $eventModel->isFacebookEvent = true;
                        $eventModel->facebookEventId = $facebookEventsModel->eventId;
                    }
                    $eventModel->pid = $calendar;
                    $eventModel->tstamp = $facebookEventsModel->updated_time;
                    $eventModel->addTime = true;
                    $eventModel->startTime = $facebookEventsModel->start_time;
                    $eventModel->endTime = $facebookEventsModel->end_time;
                    $eventModel->startDate = $facebookEventsModel->start_time;
                    $eventModel->endDate = $facebookEventsModel->end_time;
                    $eventModel->title = $facebookEventsModel->title;
                    $eventModel->teaser = nl2br($facebookEventsModel->description);
                    $eventModel->location = $facebookEventsModel->location;
                    $eventModel->ticketUri = $facebookEventsModel->ticketUri;
                    $eventModel->published = true;
                    if (! empty($facebookEventsModel->imageSrcFacebook)) {
                        $eventModel->imageSrcFacebook = $facebookEventsModel->imageSrcFacebook;
                    }

                    $eventModel->save();
                }
            }
        }

        \FbConnectorHelper::updateSessionValuesForResponse('savedEventsCount', 'savedEvents',
            $facebookEventsModel->title, $startTime, $eventWasUpdated);
    }
}
