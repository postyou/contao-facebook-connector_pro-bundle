<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;

use Facebook\Facebook;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnector;
use Postyou\ContaoFacebookConnectorBasicBundle\ContaoPersistentDataHandler;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class FbConnectorPhpSdk extends FbConnector
{
    protected $fb;

    protected $accesstoken;

    protected function initFacebookPhpSdk()
    {
        $this->fb = new Facebook(
            [
                'app_id' => parent::getAppID(),
                'app_secret' => parent::getAppSecret(),
                'default_graph_version' => parent::getVersion(),
                'persistent_data_handler' => new ContaoPersistentDataHandler(),
                'default_access_token' => parent::getAppID() . '|' . parent::getAppSecret()
            ]);
    }

    public function login()
    {
        session_start();
        $_SESSION['BE_DATA']['FBRLH_state'] = $_GET['state'];

        $helper = $this->fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();

        } catch (FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit();
        } catch (FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit();
        }

        if (isset($accessToken)) {
            $_SESSION['facebook_access_token'] = (string) $accessToken;
            $this->accesstoken = (string) $accessToken;
        } else {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit();
        }
    }

    protected function getPageAccessTokenFromFacebookAlias($facebookAlias)
    {
        $siteId = $this->fb->get($facebookAlias)->getDecodedBody()['id'];

        $facebookResponse = $this->fb->get('me/accounts', $_SESSION['facebook_access_token']);
        $accountPages = $facebookResponse->getDecodedBody()['data'];

        $pageAccessToken = '';

        foreach ($accountPages as $page) {
            if ($page['id'] === $siteId) {
                $pageAccessToken = $page['access_token'];
                break;
            }
        }

        return $pageAccessToken;
    }
}
