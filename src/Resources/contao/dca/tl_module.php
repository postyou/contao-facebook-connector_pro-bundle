<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['newslist_facebook'] = $GLOBALS['TL_DCA']['tl_module']['palettes']['newslist'];

$GLOBALS['TL_DCA']['tl_module']['palettes']['newslist_facebook'] = str_replace('skipFirst',
    'skipFirst, messageLength,showFacebookLinkAlways, hideFacebookNews', $GLOBALS['TL_DCA']['tl_module']['palettes']['newslist_facebook']);

$GLOBALS['TL_DCA']['tl_module']['palettes']['eventreader_facebook'] = $GLOBALS['TL_DCA']['tl_module']['palettes']['eventreader'];

$GLOBALS['TL_DCA']['tl_module']['palettes']['eventlist'] = str_replace('perPage',
        'perPage, messageLength', $GLOBALS['TL_DCA']['tl_module']['palettes']['eventlist']);

$GLOBALS['TL_DCA']['tl_module']['fields']['messageLength'] = array(
    'label'             => &$GLOBALS['TL_LANG']['tl_content']['messageLength'],
    'inputType'         => 'text',
    'default'           => '160',
    'eval'              => array(
                                'tl_class' => 'w50'
                            ),
    'sql'            => "varchar(255) NOT NULL default '160'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['hideFacebookNews'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['hideFacebookNews'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                      => array('tl_class' => 'w50 clr'),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['showFacebookLinkAlways'] = array(
    'label'             => &$GLOBALS['TL_LANG']['tl_content']['showFacebookLinkAlways'],
    'inputType'         => 'checkbox',
    'eval'                    => array('tl_class' => 'w50'),
    'sql'            => "char(1) NOT NULL default ''"
);
