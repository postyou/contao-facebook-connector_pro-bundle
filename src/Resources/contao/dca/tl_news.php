<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

use Contao\DataContainer;

$GLOBALS['TL_DCA']['tl_news']['config']['onload_callback'][] = array(
    'tl_news_facebook',
    'loadParentChildRecordCallback'
);

$GLOBALS['TL_DCA']['tl_news']['config']['ondelete_callback'][] = array(
    'tl_news_facebook',
    'onDelete'
);

$GLOBALS['TL_DCA']['tl_news']['list']['operations']['deleteOnFacebook'] = array(
    'label'               => &$GLOBALS['TL_LANG']['tl_news']['delete'],
    'href'                => 'key=deleteOnFacebook',
    'icon'                => 'bundles/postyoucontaofacebookconnectorbasic/img/delete_facebook.png',
    'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
);

$GLOBALS['TL_DCA']['tl_news']['fields']['facebookPostId'] = array(
    'exclude' => true,
    'eval' => array(
        'doNotCopy' => true
    ),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['facebookSitePid'] = array(
    'exclude' => true,
    'sql' => "varchar(10) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['facebookLink'] = array(
    'exclude' => true,
    'sql' => "varchar(255) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_news']['fields']['facebookPostType'] = array(
    'exclude' => true,
    'sql' => "varchar(255) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_news']['fields']['isFacebookPost'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_news']['isFacebookPost'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array(
        'tl_class' => 'clr',
        'doNotCopy' => true
    ),
    'load_callback' => array(
        array(
            'tl_news_facebook',
            'checkFacebookPost'
        )
    ),
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['imageSrcFacebook'] = array(
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['videoLink'] = array(
    'sql' => "text NULL"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['videoClass'] = array(
    'exclude' => true,
    'sql' => "varchar(256) NULL"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['videoId'] = array(
    'exclude' => true,
    'sql' => "varchar(256) NULL"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['videoThumb'] = array(
    'exclude' => true,
    'sql' => "varchar(256) NULL"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['multiSRC'] = array(
    'exclude' => true,
    'eval' => array(
        'orderField' => 'orderSRC',
        'isGallery' => true
    ),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_news']['fields']['removeHashTag'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_content']['removeHashTag'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array(
        'tl_class' => 'clr'
    ),
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_news']['palettes']['default'] = str_replace('published',
    'published, isFacebookPost', $GLOBALS['TL_DCA']['tl_news']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_news']['palettes']['default'] = str_replace('teaser',
    'teaser, removeHashTag', $GLOBALS['TL_DCA']['tl_news']['palettes']['default']);


class tl_news_facebook extends \tl_news
{
    private $otherChildRecordCallbackFunctionName;
    private $otherNewsClass;

    public function loadParentChildRecordCallback()
    {
        $GLOBALS['TL_DCA']['tl_news']['list']['operations']['edit']['button_callback'] = array(
            'tl_news_facebook',
            'customizeFacebookNewsOperations'
        );
        // $GLOBALS['TL_DCA']['tl_news']['list']['operations']['editheader']['button_callback'] = array(
        // 'tl_news_facebook',
        // 'customizeFacebookNewsOperations'
        // );
        $GLOBALS['TL_DCA']['tl_news']['list']['operations']['copy']['button_callback'] = array(
            'tl_news_facebook',
            'customizeFacebookNewsOperations'
        );
        $GLOBALS['TL_DCA']['tl_news']['list']['operations']['cut']['button_callback'] = array(
            'tl_news_facebook',
            'customizeFacebookNewsOperations'
        );
        $GLOBALS['TL_DCA']['tl_news']['list']['operations']['delete']['button_callback'] = array(
            'tl_news_facebook',
            'customizeFacebookNewsOperations'
        );
        $GLOBALS['TL_DCA']['tl_news']['list']['operations']['deleteOnFacebook']['button_callback'] = array(
            'tl_news_facebook',
            'customizeFacebookNewsOperations'
        );

        $otherNewsClassStr = null;
        if (gettype($GLOBALS['TL_DCA']['tl_news']['list']['sorting']['child_record_callback']) === 'array') {
          $this->otherChildRecordCallbackFunctionName = $GLOBALS['TL_DCA']['tl_news']['list']['sorting']['child_record_callback'][1];
          $otherNewsClassStr = $GLOBALS['TL_DCA']['tl_news']['list']['sorting']['child_record_callback'][0];
        } else {
          $this->otherChildRecordCallbackFunctionName = 'listNewsArticles';
          $otherNewsClassStr = 'tl_news';
        }

        $this->otherNewsClass = new $otherNewsClassStr();

        $GLOBALS['TL_DCA']['tl_news']['list']['sorting']['child_record_callback'] = array(
            'tl_news_facebook',
            'addFacebookIcon'
        );
    }

    public function customizeFacebookNewsOperations($row, $href, $label, $title, $icon, $attributes)
    {
        if (strpos($attributes, 'deleteOnFacebook')) {
            if ($row['isFacebookPost']) {
                \System::loadLanguageFile('tl_facebook_messages');

                $attributes = 'class="delete" onclick="if(!confirm(\'' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteConfirmNews'] . '\n' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteFacebookNews'] . '\n\n' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteToggleInfo'] .
                     '\'))return false;Backend.getScrollOffset()"';
            }
        } elseif (strpos($attributes, 'delete')) {
            if ($row['isFacebookPost']) {
                \System::loadLanguageFile('tl_facebook_messages');

                $attributes = 'class="delete" onclick="if(!confirm(\'' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteConfirmNews'] . '\n' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteFacebookNewsContao'] . '\n' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteFacebookNewsContao2'] . '\n\n' .
                     $GLOBALS['TL_LANG']['tl_facebook_messages']['deleteToggleInfo'] .
                     '\'))return false;Backend.getScrollOffset()"';
            }
        }

        return (($this->User->isAdmin || ! $row['admin']) && ! $row['isFacebookPost'] ||
             ($row['isFacebookPost'] && strpos($attributes, 'edit') === false)) ? '<a href="' .
             $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . specialchars($title) .
             '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(
                preg_replace('/\.gif$/i', '_.gif', $icon)) . ' ';
    }

    public function addFacebookIcon($arrRow)
    {
        $functionName = $this->otherChildRecordCallbackFunctionName;
        $parentReturnValue = $this->otherNewsClass->$functionName($arrRow);

        if ($arrRow['isFacebookPost'] == true) {
            return '<img title=\'facebook-news\' class="tl_news-facebook-icon" src="bundles/postyoucontaofacebookconnectorbasic/img/fb.png" alt="facebook-news"  />' .
                 $parentReturnValue;
        }

        return $parentReturnValue;
    }

    public function checkFacebookPost($varValue, DataContainer $dc)
    {
        if (! empty($dc->activeRecord->facebookPostId)) {
            \System::loadLanguageFile('tl_facebook_messages');
            $GLOBALS['TL_LANG'][$dc->table]['isFacebookPost'][0] .= ' - <em>' .
                 $GLOBALS['TL_LANG']['tl_facebook_messages']['publishedInfo'] . '</em>';

            $GLOBALS['TL_DCA'][$dc->table]['fields']['isFacebookPost']['eval']['disabled'] = true;
        }

        return $varValue;
    }

    public function onDelete($dc)
    {
        if ($dc->activeRecord->isFacebookPost && ! empty($dc->activeRecord->facebookPostId)) {
            $path = \Config::get('uploadPath') . '/facebook/posts/' . $dc->activeRecord->facebookPostId.$dc->activeRecord->pid;
            $folder = new \Folder($path);
            $folder->purge();
            $folder->delete();
        }
    }
}
