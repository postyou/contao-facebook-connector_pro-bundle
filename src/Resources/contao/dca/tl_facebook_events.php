<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_DCA']['tl_facebook_events'] = array(
    'config' => array(
        'dataContainer' => 'Table',
        'ptable' => 'tl_facebook_sites',
        'switchToEdit' => true,
        'sql' => array(
            'keys' => array(
                'id' => 'primary',
                'pid' => 'index'
            )
        ),
        'ondelete_callback' => array(
            array(
                'tl_facebook_events',
                'onDelete'
            )
        ),
        'onsubmit_callback' => array(
            array(
                'tl_facebook_events',
                'onSubmit'
            )
        )
    ),
    'list' => array(
        'sorting' => array(
            'mode' => 4,
            'fields' => array(
                "updated_time, title"
            ),
            'panelLayout' => ('filter;search,sort;limit'),
            'disableGrouping' => true,
            'headerFields' => array(
                'title'
            ),
            'child_record_callback' => array(
                'tl_facebook_events',
                'showInList'
            )
        ),
        'label' => array(
            'fields' => array(
                'title'
            ),
            'format' => '%s',
            'showColumns' => true,
            'label_callback' => array(
                'tl_facebook_events',
                'labelCallback'
            )
        ),
        'global_operations' => array(
            'all' => array(
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array(
            'edit' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['copy'],
                'href' => 'act=paste&amp;mode=copy',
                'icon' => 'copy.gif',
                'attributes' => 'onclick="Backend.getScrollOffset()"'
            ),
            'delete' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' .
                     $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] .
                     '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array(
                'label'               => &$GLOBALS['TL_LANG']['tl_facebook_posts']['toggle'],
                'icon'                => 'visible.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_facebook_events', 'toggleIcon')
            ),
            'show' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
                'attributes' => 'style="margin-right:3px"'
            )
        )
    ),
    'palettes' => array(
        'default' => '{title_legend},title, description, start_time, end_time, location, ticketUri;'
    ),
    'subpalettes' => array(),
    'fields' => array(
        'id' => array(
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array(
            'relation' => array(
                'table' => "tl_facebook_sites",
                "field" => "id",
                'type' => 'belongsTo',
                'load' => 'lazy'
            ),
            'sql' => "int(10) unsigned NOT NULL"
        ),
        'tstamp' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'created_time' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 12
        ),
        'updated_time' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 12
        ),
        'title' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['title'],
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'location' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['location'],
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'eventId' => array(
            'exclude' => true,
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'description' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['description'],
            'inputType' => 'textarea',
            'eval' => array(),
            'sql' => "text NULL"
        ),
        'ticketUri' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['ticketUri'],
            'inputType' => 'text',
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'start_time' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['start_time'],
            'inputType' => 'text',
            'default' => time(),
            'eval' => array(
                'rgxp' => 'date',
                'datepicker' => true,
                'tl_class' => 'wizard',
                'mandatory' => true
            ),
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'end_time' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['end_time'],
            'inputType' => 'text',
            'default' => time(),
            'eval' => array(
                'rgxp' => 'date',
                'datepicker' => true,
                'tl_class' => 'wizard'
            ),
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'imageSrcFacebook' => array(
            'sql' => "text NULL"
        ),
        'published' => array(
            'exclude'                 => true,
            'sql'                     => "char(1) NOT NULL default '1'"
        )
    )
);

class tl_facebook_events extends \Backend
{
    public function onSubmit($dc)
    {
        if (empty($dc->activeRecord->created_time)) {
            $this->Database->prepare("UPDATE tl_facebook_events SET created_time=? WHERE id=?")->execute(
                time(), $dc->id);
        }

        $this->Database->prepare("UPDATE tl_facebook_events SET updated_time=? WHERE id=?")->execute(
            time(), $dc->id);

    }

    public function onLoad($dc)
    {
        return;
    }

    public function onDelete($dc)
    {
        return;
    }

    public function onVersion($dc)
    {
        return;
    }

    public function labelCallback($row, $label, $dc, $args)
    {
        return $args;
    }

    public function showInList($arrRow)
    {
        $date = new DateTime();

        $date->setTimestamp($arrRow['start_time']);


        $dateEndStr = "";
        if (!empty($arrRow['end_time'])) {
            $dateEnd = new DateTime();
            $dateEnd->setTimestamp($arrRow['end_time']);
            $dateEndStr = $dateEnd->format($GLOBALS['TL_CONFIG']['datimFormat']);
        }


        return ' <em>' . $date->format($GLOBALS['TL_CONFIG']['datimFormat']) . '</em> - <em>' . $dateEndStr . '</em> ' .
             $arrRow['title'];
    }

    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }


        return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"').'</a> ';
    }


    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_article']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_article']['fields']['published']['save_callback'] as $callback) {
                if (is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->$callback[0]->$callback[1]($blnVisible, ($dc ?: $this));
                } elseif (is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }

        // Update the database
        $this->Database->prepare("UPDATE tl_facebook_posts SET tstamp=". time() .", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);
    }
}
