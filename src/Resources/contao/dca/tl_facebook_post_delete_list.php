<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_DCA']['tl_facebook_post_delete_list'] = array(
    'config' => array(
        'dataContainer' => 'Table',
        'sql' => array(
            'keys' => array(
                'id' => 'primary'
            )
        )
    ),

    'fields' => array(
        'id' => array(
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'postId' => array(
            'exclude' => true,
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'pid' => array(
            'exclude' => true,
            'sql' => "int(10) unsigned NOT NULL"
        )
    )
);
