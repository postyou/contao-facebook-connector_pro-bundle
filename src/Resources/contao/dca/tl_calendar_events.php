<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['config']['onload_callback'][] = array(
    'tl_calendar_events_facebook',
    'loadParentChildRecordCallback'
);

$GLOBALS['TL_DCA']['tl_calendar_events']['config']['ondelete_callback'][] = array(
    'tl_calendar_events_facebook',
    'onDelete'
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['facebookEventId'] = array(
    'exclude' => true,
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['isFacebookEvent'] = array(
    'exclude' => true,
    'sql' => "char(1) NOT NULL default ''"
);

\System::loadLanguageFile('tl_facebook_events');
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ticketUri'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_facebook_events']['ticketUri'],
    'inputType' => 'text',
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['imageSrcFacebook'] = array(
    'sql' => "text NULL"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default'] = str_replace('{details_legend},location,teaser;',
    '{details_legend},location,ticketUri,teaser;', $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default']);

use Postyou\ContaoFacebookConnectorProBundle\FacebookEventsModel;

class tl_calendar_events_facebook extends \tl_calendar_events
{
    private $otherChildRecordCallbackFunctionName;
    private $otherEventClass;

    public function loadParentChildRecordCallback()
    {
        $otherEventClassStr = '';
        if (gettype($GLOBALS['TL_DCA']['tl_calendar_events']['list']['sorting']['child_record_callback']) === 'array') {
            $this->otherChildRecordCallbackFunctionName = $GLOBALS['TL_DCA']['tl_calendar_events']['list']['sorting']['child_record_callback'][1];
            $otherEventClassStr = $GLOBALS['TL_DCA']['tl_calendar_events']['list']['sorting']['child_record_callback'][0];
        } else {
            $this->otherChildRecordCallbackFunctionName = 'listEvents';
            $otherEventClassStr = 'tl_calendar_events';
        }

        $this->otherEventClass = new $otherEventClassStr();

        $GLOBALS['TL_DCA']['tl_calendar_events']['list']['sorting']['child_record_callback'] = array(
            'tl_calendar_events_facebook',
            'addFacebookIcon'
        );
    }

    public function customizeFacebookNewsOperations($row, $href, $label, $title, $icon, $attributes)
    {
        return (($this->User->isAdmin || ! $row['admin']) && ! $row['isFacebookEvent'] ||
             ($row['isFacebookEvent'] && strpos($attributes, 'edit') === false)) ? '<a href="' .
             $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . specialchars($title) .
             '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(
                preg_replace('/\.gif$/i', '_.gif', $icon)) . ' ';
    }

    public function addFacebookIcon($arrRow)
    {
        $functionName = $this->otherChildRecordCallbackFunctionName;
        $parentReturnValue = $this->otherEventClass->$functionName($arrRow);

        if ($arrRow['isFacebookEvent'] == true) {
            return '<img title=\'facebook-news\' class="tl_news-facebook-icon" src="bundles/postyoucontaofacebookconnectorbasic/img/fb.png" alt="facebook-events"  />' .
                 $parentReturnValue;
        }

        return $parentReturnValue;
    }

    public function onDelete($dc)
    {
        if ($dc->activeRecord->isFacebookEvent && ! empty($dc->activeRecord->facebookEventId)) {
            $eventsModel = FacebookEventsModel::findByEventId($dc->activeRecord->facebookEventId);
            if (isset($eventsModel)) {
                $eventsModel->delete();
            }
        }

        return;
    }

}
