<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

/** Main --------------------------- **/

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['synchronizeEvents']['load_callback'] = array(array('tl_facebook_sites_pro', 'checkAvailability'));
$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['synchronizeGalleries']['load_callback'] = array(array('tl_facebook_sites_pro', 'checkAvailability'));
$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['addPostsToNewsModule']['load_callback'] = array(array('tl_facebook_sites_pro', 'checkAvailability'));

$GLOBALS['TL_DCA']['tl_facebook_sites']['config']['onload_callback'][] = array('tl_facebook_sites_pro', 'setPalettes');


/** Posts ------------------------------ **/
$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['publishPostsButton'] = array(
        'input_field_callback' => array(
                'tl_facebook_sites_pro',
                'publishPostsButton'
        ),
        'eval' => array(
                'doNotShow' => true
        )
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['newsArchiveDisplayScript'] = array(
        'input_field_callback' => array(
                'tl_facebook_sites_pro',
                'toggleNewsArchive'
        )
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['newsArchive'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['newsArchive'],
        'exclude' => true,
        'inputType' => 'checkbox',
        'foreignKey' => 'tl_news_archive.title',
        'eval' => array(
                'tl_class' => 'clr newsArchive',
                'multiple' => true,
                'submitOnChange' => true
        ),
        'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['subpalettes']['synchronizePosts'] = str_replace('addPostsToNewsModule, onlyProVersionTxt,', 'addPostsToNewsModule, newsArchive,
											newsArchiveDisplayScript,', $GLOBALS['TL_DCA']['tl_facebook_sites']['subpalettes']['synchronizePosts']);



/** Events ------------------------------- **/

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['maxEventsCount'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['maxEventsCount'],
        'exclude' => true,
        'inputType' => 'text',
        'eval' => array(
                'rgxp' => 'digit',
                'nospace' => true,
                'maxlength' => 3,
                'tl_class' => 'w50',
                'submitOnChange' => true
        ),
        'sql' => "varchar(64) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['minEventsDate'] = array(
        'exclude' => true,
        'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['minEventsDate'],
        'inputType' => 'text',
        'eval' => array(
                'rgxp' => 'date',
                'datepicker' => true,
                'tl_class' => 'wizard w50',
                'submitOnChange' => true
        ),
        'sql' => "varchar(10) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['addEventsToCalendarModule'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['addEventsToCalendarModule'],
        'exclude' => true,
        'inputType' => 'checkbox',
        'eval' => array(
                'tl_class' => 'clr',
                'submitOnChange' => true
        ),
        'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['calendarDisplayScript'] = array(
        'input_field_callback' => array(
                'tl_facebook_sites_pro',
                'toggleCalendar'
        )
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['calendar'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['calendar'],
        'exclude' => true,
        'inputType' => 'checkbox',
        'foreignKey' => 'tl_calendar.title',
        'eval' => array(
                'tl_class' => 'clr calendarArchive',
                'multiple' => true,
                'submitOnChange' => true
        ),
        'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['downloadEventsButton'] = array(
        'input_field_callback' => array(
                'tl_facebook_sites_pro',
                'downloadEventsButton'
        ),
        'eval' => array(
                'doNotShow' => true
        )
);

$GLOBALS['TL_DCA']['tl_facebook_sites']['subpalettes']['synchronizeEvents'] = 'maxEventsCount, minEventsDate,
																																							 addEventsToCalendarModule, calendar,
																																							 calendarDisplayScript, downloadEventsButton';

$GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default'] = str_replace('synchronizeEvents, onlyProVersionTxt', 'synchronizeEvents,', $GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default']);




/** Galleries ----------------------- **/

$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['galleries'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['galleries'],
    'inputType' => 'checkbox',
    'exclude' => true,
    'eval' => array(
            'multiple' => true,
            'submitOnChange' => true,
            'tl_class' => 'choose_galleries w50'
    ),
    'sql' => 'blob NULL',
    'options_callback' => array(
            'tl_facebook_sites_pro',
            'loadFacebookGalleriesFromSource'
    )
);


$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['autoSyncGalleries'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['autoSyncGalleries'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array(
            'tl_class' => 'autoSyncGalleries',
            'submitOnChange' => true
    ),
    'sql' => "char(1) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['autoSyncGalleryDisplayScript'] = array(
        'input_field_callback' => array(
                'tl_facebook_sites_pro', 'toggleAutoSyncGalleryOptions'
        ),
        'eval' => array(
                'doNotShow' => true
        )
);


$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['autoSyncGalleryOptions'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_facebook_sites']['autoSyncOptions'],
        'inputType' => 'radio',
        'options_callback' => array('tl_facebook_sites_pro', 'loadAutoSyncOptions'),
        'eval' => array('tl_class' => 'autoSyncGalleryOptions'),
        'sql' => "int(10) unsigned NOT NULL default '1'"
);


$GLOBALS['TL_DCA']['tl_facebook_sites']['fields']['downloadGalleriesButton'] = array(
        'input_field_callback' => array(
                'tl_facebook_sites_pro',
                'downloadGalleriesButton'
        ),
        'eval' => array(
                'doNotShow' => true
        )
);



$GLOBALS['TL_DCA']['tl_facebook_sites']['subpalettes']['synchronizeGalleries'] = 'galleries, autoSyncGalleries,
																																									autoSyncGalleryOptions,autoSyncGalleryDisplayScript,
																																									downloadGalleriesButton';

$GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default'] = str_replace('synchronizeGalleries, onlyProVersionTxt',
                                                                                                                                'synchronizeGalleries,',
                                                                                                                                $GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default']);



use Postyou\ContaoFacebookConnectorBasicBundle\ContaoPersistentDataHandler;
use Postyou\ContaoFacebookConnectorBasicBundle\FbConnector;
use Postyou\ContaoFacebookConnectorBasicBundle\ConnectionType;

class tl_facebook_sites_pro extends \tl_facebook_sites_basic
{

    public function loginButton($showExplanation = true)
    {
        session_start();

        $appID = \Config::get('appID');
        $appSecret = \Config::get('appSecret');

        if (empty($appID)) {
            return $this->getDisabledLoginButtonWithMessage($GLOBALS['TL_LANG']['tl_facebook_sites']['noAppIDException']);
        }

        if (empty($appSecret)) {
            return $this->getDisabledLoginButtonWithMessage($GLOBALS['TL_LANG']['tl_facebook_sites']['noAppSecretException']);
        }


        if (!class_exists('Facebook\Facebook')) {
            return $this->getDisabledLoginButtonWithMessage($GLOBALS['TL_LANG']['tl_facebook_sites']['noFacebookSDKException']);
        }

        $fb = new Facebook\Facebook (
              [
                      'app_id' => $appID,
                      'app_secret' => $appSecret,
                      'default_graph_version' => \Config::get('facebookApiVersion'),
                      'persistent_data_handler' => new ContaoPersistentDataHandler()
              ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = [
                    'manage_pages',
                    'publish_pages',
                    'publish_actions'
            ];

        $uri = substr(\Environment::get('uri'), 0,
                strpos(\Environment::get('uri'), '&')) .
                '&key=login';

        $loginUrl = $helper->getLoginUrl($uri, $permissions);

        $session = System::getContainer()->get('session');
        $session->set('redirectUri', substr(\Environment::get('request'), strpos(\Environment::get('request'), '&')));


        return ($showExplanation ? '<div class="tl_help tl_tip loginExplanation">'.
                     $GLOBALS['TL_LANG']['tl_facebook_sites']['loginExplanation'].'</div>' : '') .
                     '<div><a id="facebookLogin" class="tl_submit" href="' . htmlspecialchars($loginUrl) .
                     '">' . $GLOBALS['TL_LANG']['tl_facebook_sites']['facebookLoginLink'] . '</a></div>';
    }

    public function checkAvailability($varValue, $dc)
    {
        if (($dc->field == 'synchronizeGalleries' && class_exists('Postyou\ContaoFacebookConnectorProBundle\FbConnectorGalleryGet')
                    || ($dc->field == 'synchronizeEvents' && class_exists('Postyou\ContaoFacebookConnectorProBundle\FbConnectorEvents')
                                    || ($dc->field == 'addPostsToNewsModule' && class_exists('Postyou\ContaoFacebookConnectorProBundle\CreateFacebookModelHook'))))) {
            if (! empty($dc->activeRecord->facebookAlias)) {
                $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['eval']['class'] = '';
                $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['eval']['disabled'] = false;
            }
        }

        return $varValue;
    }

    public function getLoginButtonWithExplanation()
    {
        return $this->loginButton(true);
    }

    public function getLoginButtonWithoutExplanation()
    {
        return $this->loginButton(false);
    }

    public function publishPostsButton()
    {
        $class = "tl_submit";
        $loginExplanation = $GLOBALS['TL_LANG']['tl_facebook_sites']['loginExplanationUpload'];

        if (empty($_SESSION['facebook_access_token'])) {
            $class .= " disabled";
        }

        return '<div class="publishWrapper w50"><div id="publishPostsButton" class="publishButton"><a type="button" onclick="getPublishPostsLink(); return false;" class="' .
                     $class . '">' . $GLOBALS['TL_LANG']['tl_facebook_sites']['publishPostsButton'] .
                     '</a></div><div class="tl_help loginExplanation loginExplanationButton">' .
                     $loginExplanation . '</div>'.$this->getLoginButtonWithoutExplanation().'</div>'.parent::getButtonScript('getPublishPostsLink','publishPosts');

    }

    public function toggleNewsArchive()
    {
        return '<script>

                    var autoSyncCheckbox = document.getElementById("opt_addPostsToNewsModule_0");

                    if (autoSyncCheckbox.addEventListener) {
                        autoSyncCheckbox.addEventListener("click", toggleNewsArchive);
                        window.addEventListener("load", toggleNewsArchive);
                    } else if (autoSyncCheckbox.attachEvent) {
                        autoSyncCheckbox.attachEvent("onclick", toggleNewsArchive);
                        window.attachEvent("onload", toggleNewsArchive);
                    }

                    function toggleNewsArchive() {
                        var autoSyncEnabled = document.getElementById("opt_addPostsToNewsModule_0").checked;
                        if (autoSyncEnabled) {
                            document.getElementsByClassName("newsArchive")[0].style.display = "block";
                        } else {
                            document.getElementsByClassName("newsArchive")[0].style.display = "none";
                        }
                    }


            </script>';
    }


    public function toggleAutoSyncGalleryOptions()
    {
        return '<script>

                    var autoSyncGalleryCheckbox = document.getElementById("opt_autoSyncGalleries_0");

                    if (autoSyncGalleryCheckbox.addEventListener) {
                        autoSyncGalleryCheckbox.addEventListener("click", toggleAutoSyncGalleryOptions);
                        window.addEventListener("load", toggleAutoSyncGalleryOptions);
                    } else if (autoSyncGalleryCheckbox.attachEvent) {
                        autoSyncGalleryCheckbox.attachEvent("onclick", toggleAutoSyncGalleryOptions);
                        window.attachEvent("onload", toggleAutoSyncGalleryOptions);
                    }

                    function toggleAutoSyncGalleryOptions() {

                        var autoSyncGalleriesEnabled = document.getElementById("opt_autoSyncGalleries_0").checked;
                        if (autoSyncGalleriesEnabled) {
                            document.getElementsByClassName("autoSyncGalleryOptions")[0].style.display = "block";
                        } else {
                            document.getElementsByClassName("autoSyncGalleryOptions")[0].style.display = "none";
                        }
                    }


            </script>';
    }

    public function toggleCalendar()
    {
        return '<script>

										var addEventsToCalendarModuleCheckbox = document.getElementById("opt_addEventsToCalendarModule_0");

										if (addEventsToCalendarModuleCheckbox.addEventListener) {
												addEventsToCalendarModuleCheckbox.addEventListener("click", toggleCalendar);
												window.addEventListener("load", toggleCalendar);
										} else if (addEventsToCalendarModuleCheckbox.attachEvent) {
												addEventsToCalendarModuleCheckbox.attachEvent("onclick", toggleCalendar);
												window.attachEvent("onload", toggleCalendar);
										}

										function toggleCalendar() {
												var addToEventsModule = document.getElementById("opt_addEventsToCalendarModule_0").checked;
												if (addToEventsModule) {
														document.getElementsByClassName("calendarArchive")[0].style.display = "block";
												} else {
														document.getElementsByClassName("calendarArchive")[0].style.display = "none";
												}
										}


						</script>';
    }

    public function downloadEventsButton()
    {
        $class = "tl_submit";

        if (empty($_SESSION['facebook_access_token'])) {
            $class .= " disabled";
            $onClick = "";
        } else {
            $onClick = "onclick=\"getLoadEventsLink(); return false;\"";
        }

        $loginExplanation = $GLOBALS['TL_LANG']['tl_facebook_sites']['loginExplanation'];

        return '<div id="downloadEventsButton" class="downloadButton clr"><a type="button" '.$onClick.' class = "' .
                         $class . '">' . $GLOBALS['TL_LANG']['tl_facebook_sites']['downloadEventsButton'] . '</a></div><div class="tl_help loginExplanation loginExplanationButton">' .
                     $loginExplanation . '</div>'.$this->getLoginButtonWithoutExplanation().parent::getButtonScript('getLoadEventsLink','loadEvents');
    }


    public function downloadGalleriesButton()
    {
        $class = "tl_submit";

        return '<div class="clr"></div><div id="downloadGalleriesButton" class="downloadButton clr"><a type="button" href="#" onclick="getLoadGalleriesLink(); return false;" class = "' .
             $class . '">' . $GLOBALS['TL_LANG']['tl_facebook_sites']['downloadGalleriesButton'] . '</a></div>'.parent::getButtonScript('getLoadGalleriesLink','loadGalleries');
    }


    public function loadFacebookGalleriesFromSource($dc)
    {
        $id = \Input::get('id');

        try {
            $fbConnector = FbConnector::getInstance(
                array(
                    'connectionType' => ConnectionType::GALLERY_GET
                ));
            return $fbConnector->getGalleriesAsOptionsFromSiteId($id);
        } catch(\Exception $e) {
            return '
                <script>Backend.openModalWindow(500,\'Fehler\','.$e->getMessage().')</script>
            ';
        }

    }

        /**
         * onload_callback setPalettes
         */
        public function setPalettes($dc)
        {
            parent::setPalettes($dc);

            if (\Input::get('reportTypeEvent')) {
                $GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default'] = str_replace(
                                'synchronizeEvents', 'synchronizeEvents, reportText,',
                                $GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default']);
            } elseif (\Input::get('reportTypeGallery')) {
                $GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default'] = str_replace(
                                'synchronizeGalleries', 'synchronizeGalleries, reportText,',
                                $GLOBALS['TL_DCA']['tl_facebook_sites']['palettes']['default']);
            }
        }


        private function getDisabledLoginButtonWithMessage($message) {

            $script = '<script>
                        function showMessage(message) {
                            Backend.openModalWindow(500, "Hinweis", message);
                        }
                    </script>';

            return '<div><a id="facebookLogin" class="tl_submit disabled" onclick="showMessage(\''.$message.'\'); return false;">'
                . $GLOBALS['TL_LANG']['tl_facebook_sites']['facebookLoginLink'] . '</a></div>'.$script;
        }



}
