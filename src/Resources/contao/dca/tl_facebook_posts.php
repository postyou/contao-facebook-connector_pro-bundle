<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

use Postyou\ContaoFacebookConnectorProBundle\FacebookPostDeleteListModel;

$GLOBALS['TL_DCA']['tl_facebook_posts']['config']['ondelete_callback'] = array(array('tl_facebook_posts_pro', 'onDelete'));

class tl_facebook_posts_pro extends \tl_facebook_posts_basic
{
    public function onDelete($dc)
    {
        $facebookPostModel = parent::onDelete($dc);

        $facebookPostDeleteListModel = new FacebookPostDeleteListModel();
        $facebookPostDeleteListModel->postId = $facebookPostModel->postId;
        $facebookPostDeleteListModel->pid = $facebookPostModel->pid;
        $facebookPostDeleteListModel->save();

        return;
    }
}
