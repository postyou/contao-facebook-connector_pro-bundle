<?php
/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Postyou\ContaoFacebookConnectorProBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao Facebookconnector ContaoFacebookConnectorProBundle
 *
 * @author Mario Gienapp
 */
class PostyouContaoFacebookConnectorProBundle extends Bundle
{
}
